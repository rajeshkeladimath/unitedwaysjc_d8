(function ($) {

jQuery('.main-menu__dropdown').removeClass("is-expanded");
jQuery(".oe-mega-main-menu .dropdown.nav-level-2").hover(function(){
        jQuery(this).toggleClass("open");
	jQuery('.main-menu__dropdown').removeClass("is-expanded");
        jQuery('.main-menu__dropdown').toggleClass("is-expanded");
        });
jQuery('.menu-image-block').appendTo('.menu-item-our-focus');
jQuery( ".menu-item-our-focus" ).addClass( "" );

if ( jQuery('.mobile-main-menu .nav-level-1 li ul').text().length > 0 ) {
      jQuery( ".mobile-main-menu .nav-level-1 li ul" ).parent( ".mobile-main-menu .nav-level-1 li" ).addClass('expend');
}
jQuery( ".mobile-main-menu .expend" ).append( "<span>+</span>" );
jQuery(".mobile-main-menu .expend > ul").hide();

jQuery(".mobile-main-menu .expend span").click(function(){
  jQuery(this).parent().toggleClass("expended");
  toggleSubMenu(jQuery(this));
   return false;
});
function toggleSubMenu(thisObj){
    thisObj.prev().toggle("slow");
}

jQuery(".mobile-menu-icon .sf-accordion-toggle").on('click', function() {
  jQuery('#menuwarp .menu').removeClass("sf-hidden");
  jQuery('#menuwarp .menu').toggleClass("sf-expanded");
 });

if (jQuery(window).width() < 767) {
 jQuery('.front-page-rotor-banner .views_slideshow_cycle_main img').each(function() {
               var imgSrc = jQuery(this).attr('src');
               jQuery(this).parent().css({'background': 'url('+imgSrc+') center center no-repeat', '-webkit-background-size': '100% ', '-moz-background-size': '100%', '-o-background-size': '100%', 'background-size': '100%', '-webkit-background-size': 'cover', '-moz-background-size': 'cover', '-o-background-size': 'cover', 'background-size': 'cover'});
               jQuery(this).parent().addClass("scaleUp animation");
               jQuery(this).remove();
       });
}

})(jQuery);
